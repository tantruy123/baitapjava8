package Q2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;


/**
 * Created by tan on 2016-10-21.
 */
public class TermFrequency {
    public static void main(String[] args) {
        String str="We know that Java doesn’t provide multiple inheritance in Classes because it leads to Diamond " +
                "Problem. So how it will be handled with interfaces now, since interfaces are now similar to abstract " +
                "classes. The solution is that compiler will throw exception in this scenario and we will have to " +
                "provide implementation logic in the class implementing the interfaces tan tan.";
        //str.replaceAll("[.,]"," ");

        List<String> listString = Arrays.asList(str.split("[.,\\s]+")).stream().sorted().collect(Collectors.toList());
        Map<String,Long> mapString = listString.stream().collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        mapString.forEach((s, aLong) -> System.out.println("Key : "+s + " - value : " +aLong));
    }

    // function
    public static Map<String, Long> countWords(List<String> names) {
        return names.stream().collect(Collectors.groupingBy(name -> name, Collectors.counting()));
    }

}

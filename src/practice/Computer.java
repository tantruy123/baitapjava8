package practice;

import java.util.Date;

/**
 * Created by tan on 2016-10-21.
 */
public class Computer {
    private String id;
    private String name;
    private double price;
    private String createdDate;

    public Computer(String id, String name, String createdDate,double price) {
        this.id = id;
        this.name = name;
        this.createdDate = createdDate;
        this.price=price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "(ID : "+ id+"\nName : "+ name+"\nPrice : "+price+"\nDate : "+createdDate+")\n";
    }
}

package practice;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by tan on 2016-10-21.
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Computer> computers = new ArrayList<>();
        computers.add(new Computer("01","MSI","20/2/2011",20000));
        computers.add(new Computer("02","DELL","20/2/2011",30000));
        computers.add(new Computer("03","ASUS","20/2/2011",40000));
        computers.add(new Computer("04","SKY","20/2/2011",50000));
        computers.add(new Computer("05","APPLE","20/2/2011",60000));
        computers.add(new Computer("06","LENOVO","20/2/2011",70000));

//        Set<MyPC> myPCSet = new HashSet<>();
//        for (Computer c : computers){
//            myPCSet.add(new MyPC(c));
//        }
        // case 1
//        Set<MyPC> myPCset2 = computers.stream().map(computer -> new MyPC(computer))
//                .collect(Collectors.toSet());
//        System.out.println(myPCset2);
        // case 2
        Map<String,List<MyPC>> myPCs =
        computers.stream().map(Main::transform).collect(Collectors.groupingBy(o -> o.getComputer().getId()));
        System.out.println(myPCs);
        System.out.println("have price >50000");
        List<MyPC> list = myPCs.values().stream().map(Main::findMax).collect(Collectors.toList());
        List<MyPC> list3= list.stream().filter(myPC -> myPC!=null).collect(Collectors.toList());
        System.out.println(list3);

    }

    public static MyPC findMax(List<MyPC> list){
        double price =50000;

        for(MyPC pc :list){
            if(pc.getComputer().getPrice()>price){
                return pc;
            }
        }
        return null;
    }
    public static MyPC transform(Computer c){
        return new MyPC(c);
    }
}

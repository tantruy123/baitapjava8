package practice;

/**
 * Created by tan on 2016-10-21.
 */
public class MyPC {
    private Computer computer;
    public MyPC(){

    }
    public MyPC(Computer computer) {
        this.computer = computer;
    }

    public Computer getComputer() {
        return computer;
    }

    public void setComputer(Computer computer) {
        this.computer = computer;
    }

    @Override
    public String toString() {
        return "(ID : "+ computer.getId()+"\nName : "+ computer.getName()+"\nPrice : "+computer.getPrice()
                +"\nDate : "+computer.getCreatedDate()+")\n";
    }
}

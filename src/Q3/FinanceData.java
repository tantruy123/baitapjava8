package Q3;

/**
 * Created by tan on 2016-10-21.
 */
public class FinanceData {
    private String OID;
    private String country;
    private int year;
    private String description;
    private String magnitude;
    private Long value;

    public FinanceData(String OID, String country, int year, String description, String magnitude, Long value) {
        this.OID = OID;
        this.country = country;
        this.year = year;
        this.description = description;
        this.magnitude = magnitude;
        this.value = value;

    }

    public String getOID() {
        return OID;
    }

    public void setOID(String OID) {
        this.OID = OID;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(String magnitude) {
        this.magnitude = magnitude;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "("+OID+ ","+country+ ","+year+ ","+description+ ","+magnitude+ ","+value+ ")";
    }
}

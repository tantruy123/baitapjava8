package Q3;

import sun.misc.IOUtils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by tan on 2016-10-21.
 */
public class Main {

 public static  ArrayList<FinanceData> listData = new ArrayList<>();
    public static void main(String[] args) {
        String fileName = "UNdata_Export.csv";
        //read file into stream, try-with-resources
        // replace


        // read file
        try {
            Path path = Paths.get(fileName);
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

            for (int i = 1; i < lines.size() - 1; i++) {
                // get line
                List<String> stringList = Arrays.asList(lines.get(i).split(",")).stream().collect(Collectors.toList());
                listData.add(getFinaceDate(stringList));

            }

        } catch (IOException e) {

        }

        listData.forEach(System.out::println);

    }

    public  ArrayList<FinanceData> getListData() {
        ArrayList<FinanceData> listData = new ArrayList<>();
        String fileName = "UNdata_Export.csv";
        //read file into stream, try-with-resources
        // replace


        // read file
        try {
            Path path = Paths.get(fileName);
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

            for (int i = 1; i < lines.size() - 1; i++) {
                // get line
                List<String> stringList = Arrays.asList(lines.get(i).split(",")).stream().collect(Collectors.toList());
                listData.add(getFinaceDate(stringList));

            }

        } catch (IOException e) {

        }

       return listData;
    }

    public static FinanceData getFinaceDate(List<String> stringList) {
        if (stringList.size() == 0) {
            return null;
        }
        String iod,country,description,magnitude;
        int year;
        Long value;
        if(stringList.size()==6) {
            String pattern = "\"";
            replaceSepicalcharacter rp = (special, input) -> {
                return input.replaceAll("\"", "");
            };
            System.out.println(stringList);
             iod = rp.remove(pattern, stringList.get(0));
             country = rp.remove(pattern, stringList.get(1));
            try {
                year = Integer.parseInt(rp.remove(pattern, stringList.get(2)));
            } catch (Exception e) {
                year = 0;
            }
             description = rp.remove(pattern, stringList.get(3));
             magnitude = rp.remove(pattern, stringList.get(4));
            try {
                value = Long.parseLong(rp.remove(pattern, stringList.get(5)));
            } catch (Exception e) {
                double x = Double.parseDouble(rp.remove(pattern, stringList.get(5)));
                value = (long) x;
            }
        }else{
            String pattern = "\"";
            replaceSepicalcharacter rp = (special, input) -> {
                return input.replaceAll("\"", "");
            };
            System.out.println(stringList);
             iod = rp.remove(pattern, stringList.get(0));
             country = rp.remove(pattern, stringList.get(1))+","+rp.remove(pattern,stringList.get(2));
            try {
                year = Integer.parseInt(rp.remove(pattern, stringList.get(3)));
            } catch (Exception e) {
                year = 0;
            }
             description = rp.remove(pattern, stringList.get(4));
             magnitude = rp.remove(pattern, stringList.get(5));
            try {
                value = Long.parseLong(rp.remove(pattern, stringList.get(6)));
            } catch (Exception e) {
                double x = Double.parseDouble(rp.remove(pattern, stringList.get(6)));
                value = (long) x;
            }
        }
        return new FinanceData(iod, country, year, description, magnitude, value);
    }

    public static String replaceSepicalcharacter(String s) {

        return s.replaceAll("\"", "");
    }

    interface replaceSepicalcharacter {
        String remove(String special, String input);
    }

}

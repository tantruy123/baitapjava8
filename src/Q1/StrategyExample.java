package Q1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tan on 2016-10-21.
 */
public class StrategyExample {

    public static void main(String[] args) {
        Strategy plus = (a, b) -> {
            System.out.println("Called ConcreteStrategy plus's execute()");
            System.out.print("Sum "+ a+" + "+b + " = ");
            return a + b;
        };
        Strategy minus = (a, b) -> {
            System.out.println("Called ConcreteStrategy minus's execute()");
            System.out.print("Sum "+ a+" - "+b + " = ");
            return a - b;
        };
        Strategy multi = (a, b) -> {
            System.out.println("Called ConcreteStrategy multiplication 's execute()");
            System.out.print("Sum "+ a+" * "+b + " = ");
            return a * b;
        };
//        // sum 3 4
//        System.out.println();
//        int result = plus.excute(3, 4);
//        System.out.println("Sum 3 + 4 = " + result);
//        // minus
//        result = minus.excute(3, 4);
//        System.out.println("Sum 3 - 4 = " + result);
//        // mutiplication
//        result = multi.excute(3, 4);
//        System.out.println("Sum 3 * 4 = " + result);
        List<Strategy> list = Arrays.asList(plus,minus,multi);
        for (Strategy s : list){
            System.out.println(s.excute(3,4));
        }
    }

    public class Context {
        private Strategy strategy;

        public Context(Strategy strategy) {
            this.strategy = strategy;
        }

        public int excute(int a, int b) {
            return strategy.excute(a, b);
        }
    }

    public interface Strategy {
        int excute(int a, int b);
    }
}

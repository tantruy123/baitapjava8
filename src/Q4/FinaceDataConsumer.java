package Q4;

import Q3.FinanceData;
import sun.rmi.runtime.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingDeque;

/**
 * Created by tan on 2016-10-21.
 */
public class FinaceDataConsumer extends Thread {

    private BlockingDeque<FinanceData> queue;

    public FinaceDataConsumer(BlockingDeque<FinanceData> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while(true) {
            try {
                System.out.println(this.getName() + " is waiting. ....");
                FinanceData f = queue.take();
                consum(f);
            } catch (Exception e) {
                handlerException(e);
            }
        }
    }

    private void handlerException(Exception e) {
    }

    private void consum(FinanceData f) {
        System.out.println(this.getName()+ " take " + f.getOID());
        Report.put(f.getCountry(),f.getValue());

    }
}


package Q4;

import Q3.FinanceData;
import Q3.Main;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static Q3.Main.getFinaceDate;

/**
 * Created by tan on 2016-10-21.
 */
public class FinaceDataProducer extends Thread {
    ArrayList<FinanceData> listData;
    BlockingDeque<FinanceData> queue;

    public FinaceDataProducer(BlockingDeque<FinanceData> queue) {
        this.queue = queue;
    }
    @Override
    public void run() {
       listData = new Main().getListData();
        for (FinanceData f : listData) {
            System.out.println();
            queue.offer(f);
        }
        //listData.parallelStream().forEach(financeData -> queue.offer(financeData));
        report();
    }
    private void report() {
        Report.display();
    }

}

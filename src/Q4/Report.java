package Q4;

import Q3.FinanceData;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tan on 2016-10-21.
 */
public class Report {
   static Map<String,Long>  map= new HashMap<>();


    public static void put(String country, Long vaLong){
        if(map.containsKey(country)){
            map.put(country,map.get(country)+vaLong);
        }else{
            map.put(country,vaLong);
        }
    }
    public static void display(){
        map.forEach((s, aLong) -> {
            System.out.println(s+ " : "+aLong);
        });
    }
}

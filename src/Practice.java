import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by tan on 2016-10-20.
 */
public class Practice {
    public static void main(String[] args) {
        // declare an instance off an interface
        MathOperation sum =(a, b) -> {return a+b;};
        MathOperation subtraction =(a, b) -> {return a-b;};
        MathOperation multiplication =(a, b) -> {return a*b;};
        MathOperation division =(a, b) -> {return a/b;};
        System.out.println("addition : "+sum.operation(3,5));
        System.out.println("subtraction : "+subtraction.operation(3,5));
        System.out.println("Multiplication : "+multiplication.operation(3,5));
        System.out.println("Division : "+division.operation(3,5));
        System.out.println();
        AnBeoSida<Character,Integer> anBeoSida2 =Practice::print2;
        anBeoSida2.getASCII('A');


        // using funtional interface , compare able interface and println

        List<String> list =Produce.getList();

        Collections.sort(list,Practice::sorting);
        System.out.println("list after sorting : ");
        list.forEach(System.out::println);
        //


        // using funtional interface
        //case 1
        System.out.println("UpperCase start With  d : case 1 ");
        list.replaceAll(Practice::upperCaseWith);
        StringBuilder sb = new StringBuilder();
        list.forEach(sb::append);
        System.out.println(sb);
        //case 2
        System.out.println("UpperCase start With  d :  case 2");
        list.stream().filter(s -> s.startsWith("d")).map(String::toUpperCase).forEach(System.out::println);
        // using consumer
        System.out.println("Using consumer : ");
        Consumer<String> listConsumer =Practice::printName;
        listConsumer.accept("tan1");
        listConsumer.accept("tan2");
        listConsumer.accept("tan3");
        listConsumer.accept("tan4");

        // upper case start with character d
        // case 3
        System.out.println("\nUpperCase start With  d :  case 3");
        list.forEach(Practice::isUpperCaseWith);


    }
    public static void  printName(String name){
        System.out.print(name + " ");
    }
    static int sorting(String str1 ,String str2){
        return str1.compareTo(str2);
    }
    static void print2(char n){
        System.out.println(n+" to ASCII : " + (int)n);
    }
    interface MathOperation{
        int operation(int a, int b);

    }
    interface AnBeoSida<T,P>{
        void getASCII(T t);
    }
    static String upperCaseWith(String s){
        if(s.startsWith("d")){
            return s.toUpperCase();
        }
        return s;
    }
    public static void isUpperCaseWith(String s) {
        if (s.startsWith("d")) {
            System.out.print(s.toUpperCase()+ " ");
        } else {
            System.out.print(s+ " ");
        }
    }


}
